package org.geonames.converting.cities.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;

import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Artyom Fyodorov
 */
public class CityResultHolderTest {
    private final int totalResultsCount = 1;
    private final String longitude = "2.3488";
    private final long geonameId = 2988507L;
    private final String countryCode = "FR";
    private final String name = "Paris";
    private final String toponymName = "Paris";
    private final String latitude = "48.85341";
    private final String facility = "P";
    private final String featureCode = "PPLC";

    private Jsonb jsonb;
    private String jsonSource;

    @BeforeEach
    void setUp() throws Exception {
        this.jsonb = JsonbBuilder.create();
        this.jsonSource = "{\n" +
                "\"totalResultsCount\": " + totalResultsCount + ",\n " +
                "\"geonames\": [\n" +
                "{\n" +
                "\"lng\": \"" + longitude + "\",\n" +
                "\"geonameId\": " + geonameId + ",\n " +
                "\"countryCode\": \"" + countryCode + "\",\n" +
                "\"name\": \"" + name + "\",\n" +
                "\"toponymName\": \"" + toponymName + "\",\n" +
                "\"lat\": \"" + latitude + "\",\n" +
                "\"fcl\": \"" + facility + "\",\n" +
                "\"fcode\": \"" + featureCode + "\"\n" +
                "}\n" +
                "]\n" +
                "}";
    }

    @Test
    public void successfulDeserializeJsonContent() throws Exception {

        final City expectedCity = new City();
        expectedCity.setLongitude(longitude);
        expectedCity.setGeonameId(geonameId);
        expectedCity.setCountryCode(countryCode);
        expectedCity.setName(name);
        expectedCity.setToponymName(toponymName);
        expectedCity.setLatitude(latitude);
        expectedCity.setFacility(facility);
        expectedCity.setFeatureCode(featureCode);

        final CityResultHolder expected = new CityResultHolder();
        expected.setTotalResultsCount(totalResultsCount);
        expected.setCities(singletonList(expectedCity));

        final CityResultHolder actual = jsonb.fromJson(jsonSource, CityResultHolder.class);
        assertThat(actual, is(expected));
    }
}