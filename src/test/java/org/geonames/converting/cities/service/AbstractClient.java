package org.geonames.converting.cities.service;

import org.junit.jupiter.api.BeforeEach;

/**
 * @author Artyom Fyodorov
 */
public abstract class AbstractClient {

    static final String CITY_NAME = "new york city";
    GeonamesApiClient cut;

    @BeforeEach
    void setUp() {
        this.cut = new GeonamesApiClient();
    }
}
