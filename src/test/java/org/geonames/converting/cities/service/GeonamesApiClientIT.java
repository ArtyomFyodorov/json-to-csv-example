package org.geonames.converting.cities.service;

import org.geonames.converting.cities.domain.City;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Artyom Fyodorov
 */
class GeonamesApiClientIT extends AbstractClient {

    @Test
    void testFindShortGeonamesByCity() {
        List<City> result = this.cut.findShortGeonamesByCity(CITY_NAME);

        assertTrue(result.size() > 0);
    }
}