package org.geonames.converting.cities.service;

import org.junit.jupiter.api.Test;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

/**
 * @author Artyom Fyodorov
 */
class GeonamesApiClientTest extends AbstractClient {

    @Test
    void testGetUriTemplate() throws Exception {

        final String translatedCity = URLEncoder.encode(CITY_NAME, StandardCharsets.UTF_8.name());
        final String actualUriTemplate = this.cut.getUriTemplate(CITY_NAME);

        System.out.printf("Translated city: %s%nURI template: %s%n", translatedCity, actualUriTemplate);
        assertThat(actualUriTemplate, containsString(translatedCity));
    }
}