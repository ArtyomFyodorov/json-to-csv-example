package org.geonames.converting.cities.service;

import org.geonames.converting.cities.dto.CityDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author Artyom Fyodorov
 */
public class CsvGeonamesWriterTest {

    private CsvGeonamesWriter gut;

    @BeforeEach
    public void setUp() throws Exception {
        this.gut = new CsvGeonamesWriter();
    }

    @Test
    public void testDoWrite() throws Exception {
        final String expected = "countryCode,name,toponymName,lng,lat\n" +
                "FR,Paris,Paris,2.3488,48.85341\n" +
                "US,Paris,Paris,-95.55551,33.66094\n" +
                "DK,Paris,Paris,8.48996,56.51417\n";

        final List<CityDto> data = Arrays.asList(
                new CityDto("FR", "Paris", "Paris", "2.3488", "48.85341"),
                new CityDto("US", "Paris", "Paris", "-95.55551", "33.66094"),
                new CityDto("DK", "Paris", "Paris", "8.48996", "56.51417")
        );

        final StringWriter actualWriter = new StringWriter();
        assertNotNull(this.gut);
        gut.doWrite(new PrintWriter(actualWriter), data);

        assertThat(actualWriter.toString(), is(expected));
    }


}