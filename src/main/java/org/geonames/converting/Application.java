package org.geonames.converting;

import org.geonames.converting.cities.service.CsvGeonamesConverter;
import org.geonames.converting.cities.service.CsvGeonamesWriter;
import org.geonames.converting.cities.service.GeonamesApiClient;

import java.util.Collections;
import java.util.Scanner;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

/**
 * @author Artyom Fyodorov
 */
public class Application {
    private static final CsvGeonamesWriter csvGeonamesWriter;
    private static final CsvGeonamesConverter csvGeonamesConverter;
    private static final GeonamesApiClient geonamesApiClient;

    static {
        csvGeonamesWriter = new CsvGeonamesWriter();
        csvGeonamesConverter = new CsvGeonamesConverter();
        geonamesApiClient = new GeonamesApiClient();
    }

    public static void main(String... args) {
        try {
            final String requestedCityName = getRequestedCity(args);
            final String fileName = requestedCityName.replace(" ", "_") + ".csv";

            csvGeonamesWriter.writeToFile(fileName,
                    geonamesApiClient.findShortGeonamesByCity(requestedCityName)
                            .stream()
                            .map(csvGeonamesConverter::toCityDto)
                            .collect(collectingAndThen(toList(), Collections::unmodifiableList)));

        } catch (RuntimeException e) {
            System.err.println("An error occurred while executing the request");
            e.printStackTrace();
        }
        System.out.println("\nDone\n");
    }

    private static String getRequestedCity(String... args) {
        String result;
        if (args.length != 1) {
            printWelcomeScreen();
            result = new Scanner(System.in)
                    .nextLine().trim().toLowerCase();
        } else {
            result = args[0].replaceAll("_", " ").trim().toLowerCase();
        }

        return result;
    }

    private static void printWelcomeScreen() {
        System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
        System.out.println("You can specify a name of the city when the program is started:");
        System.out.println("  Syntax: java -jar target/geonames-json-to-csv-jar-with-dependencies.jar CITY_NAME");
        System.out.println("  Example: java -jar target/geonames-json-to-csv-jar-with-dependencies.jar praga");
        System.out.println("  Example: java -jar target/geonames-json-to-csv-jar-with-dependencies.jar new_york_city");
        System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
        System.out.println("\nEnter your city:");
    }
}
