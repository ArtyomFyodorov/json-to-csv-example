package org.geonames.converting.cities.service;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.NonNull;
import org.geonames.converting.cities.dto.CityDto;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Collection;

import static java.nio.file.Files.newBufferedWriter;
import static java.nio.file.Paths.get;

/**
 * @author Artyom Fyodorov
 */
public class CsvGeonamesWriter {
    private final CsvMapper csvMapper;
    private final CsvSchema csvSchema;

    public CsvGeonamesWriter() {
        this.csvMapper = new CsvMapper();
        this.csvSchema = csvMapper
                .schemaFor(CityDto.class)
                .withHeader()
                .sortedBy("countryCode", "name", "toponymName", "lng", "lat");
    }

    public void writeToFile(@NonNull String fileName, @NonNull Collection<CityDto> data) {

        try (Writer writer = new PrintWriter(newBufferedWriter(get(fileName)))) {
            doWrite(writer, data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    void doWrite(Writer writer, Collection<CityDto> data) throws IOException {
        this.csvMapper.writer().with(this.csvSchema).writeValues(writer).writeAll(data);
    }
}
