package org.geonames.converting.cities.service;

import lombok.NonNull;
import org.geonames.converting.cities.domain.City;
import org.geonames.converting.cities.dto.CityDto;

/**
 * @author Artyom Fyodorov
 */
public class CsvGeonamesConverter {

    public CityDto toCityDto(@NonNull City in) {

        CityDto dto = new CityDto();

        dto.setCountryCode(in.getCountryCode());
        dto.setName(in.getName());
        dto.setToponymName(in.getToponymName());
        dto.setLongitude(in.getLongitude());
        dto.setLatitude(in.getLatitude());

        return dto;
    }
}
