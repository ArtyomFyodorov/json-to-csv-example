package org.geonames.converting.cities.service;

import lombok.NonNull;
import org.geonames.converting.cities.domain.City;
import org.geonames.converting.cities.domain.CityResultHolder;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import static java.nio.charset.StandardCharsets.UTF_8;
import static javax.ws.rs.core.Response.Status.Family.SUCCESSFUL;

/**
 * @author Artyom Fyodorov
 */
public class GeonamesApiClient {
    private static final Logger logger = Logger.getLogger(GeonamesApiClient.class.getName());

    private static Client client = ClientBuilder.newClient();

    public List<City> findShortGeonamesByCity(@NonNull String cityName) {

        final Response response = client.target(this.getUri(cityName)).request().get();
        if (response.getStatusInfo().getFamily() != SUCCESSFUL) {
            throw new RuntimeException("Failed: response status code " + response.getStatus());
        }

        return response.readEntity(CityResultHolder.class).getCities();
    }

    private URI getUri(@NonNull String cityName) {
        final String uriTemplate = this.getUriTemplate(cityName);

        return UriBuilder.fromUri(uriTemplate).build();
    }

    String getUriTemplate(String cityName) {
        String uriTemplate = this.loadTemplate();
        try {
            uriTemplate = uriTemplate.replace("{city}", URLEncoder.encode(cityName, UTF_8.name()));
        } catch (UnsupportedEncodingException ignore) {/*NOP*/}

        return uriTemplate;
    }

    private String loadTemplate() {
        Properties props = new Properties();
        try (InputStream in = this.getClass().getClassLoader().getResourceAsStream("application.properties")) {
            props.load(in);
        } catch (IOException e) {
            logger.severe("The properties file could not be loaded.");
            throw new RuntimeException(e);
        }

        return props.getProperty("geonames.uri-template");
    }

}
