package org.geonames.converting.cities.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Artyom Fyodorov
 */
@Data @NoArgsConstructor @AllArgsConstructor
public class CityDto {

    private String countryCode;
    private String name;
    private String toponymName;
    @JsonProperty("lng")
    private String longitude;
    @JsonProperty("lat")
    private String latitude;
}
