package org.geonames.converting.cities.domain;

import lombok.Data;

import javax.json.bind.annotation.JsonbProperty;

/**
 * @author Artyom Fyodorov
 */
@Data
public class City {

    @JsonbProperty("lng")
    private String longitude;
    private Long geonameId;
    private String countryCode;
    private String name;
    private String toponymName;
    @JsonbProperty("lat")
    private String latitude;
    @JsonbProperty("fcl")
    private String facility;
    @JsonbProperty("fcode")
    private String featureCode;
}
