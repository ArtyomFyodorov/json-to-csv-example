package org.geonames.converting.cities.domain;

import lombok.Data;

import javax.json.bind.annotation.JsonbProperty;
import java.util.List;

/**
 * @author Artyom Fyodorov
 */
@Data
public class CityResultHolder {

    private Integer totalResultsCount;
    @JsonbProperty("geonames")
    private List<City> cities;
}
