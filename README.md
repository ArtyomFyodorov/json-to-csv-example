# Json to CSV

* This is a solution for [Acme Corporation](https://en.wikipedia.org/wiki/Acme_Corporation) development test. 
* This console application requests data for a specific city in the JSON format. The received data is converted to CSV format and saved to a file. 
* This example works with [http://www.geonames.org/](http://www.geonames.org/)

## Usage

### Build
```text
mvn clean package
```

### Start
```text
java -jar target/geonames-json-to-csv-jar-with-dependencies.jar
```
Also you can specify a name of the city when the program is started.

* Example #1: 
```text
java -jar target/geonames-json-to-csv-jar-with-dependencies.jar praga
```

City names consisting of several words should be separated by an underscore.

* Example #2: 
```text
java -jar target/geonames-json-to-csv-jar-with-dependencies.jar new_york_city
```